package mayra.wendy.meriles.acha.ci5545111;

public class list {
 private cola raiz;
    private cola siguiente = null;
    private int cant = 0;
   private boolean vacia()
   {
      return siguiente == null; 
   }
   public void insertar (Integer dato)
   {
       cola cl = new cola(dato);
       inserta(cl);
   }
   public void insertar (String dato)
   {
       cola cl = new cola(dato);
       inserta(cl);
   }
   public void insertar (float dato)
   {
       cola cl = new cola(dato);
       inserta(cl);
   }
   public void insertar (boolean dato)
   {
       cola cl = new cola(dato);
       inserta(cl);
   }
   private void inserta(cola cl)
   {
       if(vacia())
       {
           raiz = cl;
           siguiente = cl;
       }
       else
       {
           cl.setEnlace(siguiente);
           siguiente = cl;
           raiz = siguiente;
       }
       cant++;
   }
   public void eliminar(Object dato)
   {
       cola aux = raiz;
       siguiente = raiz;
       int cont = 0;
       while(siguiente!=null)
       {
           if(siguiente.getDato().equals(dato))
           {
               cont++;
               break;
           }
           siguiente = siguiente.getEnlace();
       }
       if(cont > 0)
       {
           aux=siguiente;
           aux.setEnlace(siguiente.getEnlace());
           System.out.println("el dato "+aux.getDato().toString()+ " fue eliminado");
           siguiente=null;
           cant--;
       }
   }
   public void buscar(Object dato)
   {
       siguiente = raiz;
       int cont = 0;
       while(siguiente!=null)
       {
           if(siguiente.getDato().equals(dato))
           {
               System.out.println("existe el " + siguiente.getDato().toString()+" elemento en la cola");
               cont++;
           }
           siguiente = siguiente.getEnlace();
       }
       if(cont == 0)
       {
           System.out.println("no existe el elemeto en la cola");
       }
   }
   public int cantidad ()
   {
       return cant;
   }
   public void maxmin()
   {
       siguiente=raiz;
       cola aux = raiz;
       while(siguiente!=null)
       {
           aux = siguiente;
           siguiente = siguiente.getEnlace();
       }
       System.out.println(aux.getDato().toString()+" "+raiz.getDato().toString());
   }
   public void mostrar()
   {
       siguiente = raiz;
       while(siguiente!=null)
       {
           System.out.print(siguiente.getDato().toString()+ " ");
           siguiente = siguiente.getEnlace();
       }
   }
}
